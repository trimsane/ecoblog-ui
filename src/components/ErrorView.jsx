import React, { Fragment } from 'react';
import { Row } from 'antd';

const ErrorView = ({ msg }) => (
  <Fragment>
    <Row>
    <h1>Ups something went wrong...</h1>
    </Row>
    <Row>
     <h4>{`Reason: ${msg}`}</h4>
    </Row>
  </Fragment>
);

export default ErrorView;
