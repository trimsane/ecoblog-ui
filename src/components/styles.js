import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Title = styled.span`
  font-size: 25px;
  font-family: 'Raleway';
  font-weight: 900;
  color: #626065;
`;

export const Timestamp = styled.span`
  font-size: 14px;
  font-weight: 400;
  color: grey;
  padding: 10px 0;
`;

export const Content = styled.p`
  font-size: 15px;
  color: #626065;
  font-weight: 400;
`;

export const Button = styled(Link)`
  width: 200px;
  padding: 10px;
  background-color: #1890ff;
  border-radius: 100px;
  color: #ffffff;
  font-family: Raleway;
  font-size: 13px;
`;

export const Avatar = styled.img`
  width: 30px;
  height: 30px;
  margin-right: 10px;
  margin-bottom: 10px;
`;