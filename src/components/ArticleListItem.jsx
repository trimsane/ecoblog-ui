import React from 'react';
import { List, Row, Col, Skeleton } from 'antd';
import { Link } from 'react-router-dom';
import { Title, Timestamp, Content } from './styles';
import { timestampToDateString } from '../utils/date';

const ArticleListItem = ({ loading, item }) => {
  return (
    <List.Item>
      <Skeleton active paragraph={{ rows: 10 }} loading={loading}>
        {!loading && (
          <article>
            <Row type="flex" justify="space-between" align="center">
              <Col span={10}>
                <Title>{item.title}</Title>
              </Col>
              <Col span={6}>
                <Timestamp>{timestampToDateString(item.createdAt)}</Timestamp>
              </Col>
            </Row>
            <Row>
              <Content>{item.intro}</Content>
              <Link to={`/articles/${item.id}`}>Read more >></Link>
            </Row>
          </article>
        )}
      </Skeleton>
    </List.Item>
  );
};

export default ArticleListItem;
