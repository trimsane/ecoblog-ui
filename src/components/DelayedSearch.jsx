import React, { Component } from 'react';
import { Input } from 'antd';

const Search = Input.Search;

class DelayedSearch extends Component {
  state = {};

  render() {
    return (
      <Search
        placeholder="Input search text"
        onChange={this.handleChange}
      />
    );
  }

  handleChange = e => {
    const { delay, onChange } = this.props;

    if (this.state.typingTimeout) {
      clearTimeout(this.state.typingTimeout);
    }

    this.setState(({
      query: e.target.value,
      typingTimeout: setTimeout(() => {
        const query = this.state.query;

        if (!query || query === '') {
          return;
        }
        onChange(this.state.query);
      }, delay)
    }));
  };
}

export default DelayedSearch;
