import * as Api from '../services/api';

export const FETCH_BLOGITEMS_REQUEST = 'FETCH_BLOGITEM_REQUEST';
export const FETCH_BLOGITEMS_FAIL = 'FETCH_BLOGITEM_FAIL';
export const FETCH_BLOGITEM_SUCCESS = 'FETCH_BLOGITEM_SUCCESS';

export const fetchBlogItemsPage = (page, pageSize) => dispatch => {
  dispatch({
    type: FETCH_BLOGITEMS_REQUEST,
    page,
  });

  Api.fetchBlogItems(pageSize, (page - 1) * pageSize)
    .then(response =>
      dispatch({
        type: FETCH_BLOGITEM_SUCCESS,
        payload: response.payload,
        page,
      })
    )
    .catch(error =>
      dispatch({
        type: FETCH_BLOGITEMS_FAIL,
        page,
        error,
      })
    );
};

export const SEARCH_BLOGITEMS_REQUEST = 'SEARCH_BLOGITEMS_REQUEST';
export const SEARCH_BLOGITEMS_FAIL = 'SEARCH_BLOGITEMS_FAIL';
export const SEARCH_BLOGITEMS_SUCCESS = 'SEARCH_BLOGITEMS_SUCCESS';

export const searchBlogItems = query => dispatch => {
  dispatch({
    type: SEARCH_BLOGITEMS_REQUEST
  });

  Api.searchBlogItems(query)
    .then(response =>
      dispatch({
        type: SEARCH_BLOGITEMS_SUCCESS,
        payload: response.payload
      })
    )
    .catch(err =>
      dispatch({
        type: SEARCH_BLOGITEMS_FAIL,
        err
      })
    );
};

export const FETCH_ARTICLE_REQUEST = 'FETCH_ARTICLE_REQUEST';
export const FETCH_ARTICLE_FAIL = 'FETCH_ARTICLE_FAIL';
export const FETCH_ARTICLE_SUCCESS = 'FETCH_ARTICLE_SUCCESS';

export const fetchArticle = id => dispatch => {
  dispatch({
    type: FETCH_ARTICLE_REQUEST,
    id
  });

  Api.fetchBlogItem(id)
  .then(response =>
    dispatch({
      type: FETCH_ARTICLE_SUCCESS,
      payload: response.payload
    })
  )
  .catch(error =>
    dispatch({
      type: FETCH_ARTICLE_FAIL,
      error
    })
  );
}