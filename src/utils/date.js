
export const timestampToDate = (ts) => {
  return new Date(ts * 1000)
}

export const timestampToDateString = (ts) => {
  return timestampToDate(ts).toLocaleString()
}