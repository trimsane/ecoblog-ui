import {
  FETCH_BLOGITEMS_REQUEST,
  FETCH_BLOGITEM_SUCCESS,
  SEARCH_BLOGITEMS_REQUEST,
  SEARCH_BLOGITEMS_FAIL,
  SEARCH_BLOGITEMS_SUCCESS,
  FETCH_BLOGITEMS_FAIL,
  FETCH_ARTICLE_REQUEST,
  FETCH_ARTICLE_SUCCESS,
  FETCH_ARTICLE_FAIL
} from '../actions/blogActions';
import { combineReducers } from 'redux';

const INDEX_PAGE_SIZE_DEFAULT = 10;

const blogItemsInitState = {
  meta: {
    page: 1,
    pageSize: INDEX_PAGE_SIZE_DEFAULT,
    total: 0
  }
};

const blogItemsReducer = (blogItems = blogItemsInitState, action = {}) => {
  switch (action.type) {
    case FETCH_BLOGITEMS_REQUEST:
      return {
        ...blogItems,
        meta: {
          ...blogItems.meta,
          page: action.page
        },
        [action.page]: {
          data: [],
          loading: true
        }
      };
    case FETCH_BLOGITEM_SUCCESS: {
      const { total, result } = action.payload;
      return {
        ...blogItems,
        meta: {
          ...blogItems.meta,
          total
        },
        [action.page]: {
          data: result,
          loading: false
        }
      };
    }

    case FETCH_BLOGITEMS_FAIL: {
      return {
        ...blogItems,
        [action.page]: {
          error: action.error,
          loading: false
        }
      };
    }

    default:
      return blogItems;
  }
};

const searchState = {
  query: '',
  results: []
};

const searchBlogItemsReducer = (search = searchState, action) => {
  switch (action.type) {
    case SEARCH_BLOGITEMS_REQUEST:
      return {
        ...search,
        query: action.query
      };
    case SEARCH_BLOGITEMS_SUCCESS:
      return {
        ...search,
        results: action.payload || []
      };
    case SEARCH_BLOGITEMS_FAIL:
      return {
        ...search,
        results: []
      };
    default:
      return search;
  }
};

const selectedArticleReducer = (selected = {}, action) => {
  switch (action.type) {
    case FETCH_ARTICLE_REQUEST:
      return {
        ...selected,
        loading: true
      };
    case FETCH_ARTICLE_SUCCESS: {
      return {
        ...selected,
        article: action.payload,
        loading: false
      };
    }

    case FETCH_ARTICLE_FAIL: {
      return {
        ...selected,
        error: action.error,
        loading: false
      };
    }

    default:
      return selected;
  }
};

export default combineReducers({
  items: blogItemsReducer,
  search: searchBlogItemsReducer,
  selected: selectedArticleReducer,
});
