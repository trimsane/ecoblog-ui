import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import blogReducer from './blogreducer'

const rootReducer = combineReducers({
  routing: routerReducer,
  blog: blogReducer,
});

export default rootReducer;