import styled from 'styled-components';
import { Layout } from 'antd';

export const Logo = styled.img`
  position: absolute;
  right: 50px;
  top: 15px;
`;

export const LayoutWrapper = styled(Layout)`
  width: 80%;
  margin: auto;
  position: relative;
  background-color: #f9f9f9;
`;

export const ContentWrapper = styled(Layout.Content)`
  background: #fff;
  padding: 24px;
  min-height: 300px;
`;

export const FooterText = styled.div`
  text-align: center;
`;