import React, { Fragment } from 'react';
import { Route, Link, Switch, Redirect } from 'react-router-dom';

import { Layout, Menu } from 'antd';

import BlogRouter from '../Blog/';
import ErrorView from '../../components/ErrorView';
import { Logo, LayoutWrapper, FooterText, ContentWrapper } from './styles';

const { Header, Footer } = Layout;

const App = () => (
  <LayoutWrapper>
    <Header>
      <Logo />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">
          <Link to="/blog/pages/1">Blog</Link>
        </Menu.Item>
      </Menu>
    </Header>
    <ContentWrapper>
      <Switch>
        <Redirect exact path="/" to="/blog" />
        <BlogRouter />
        <Route component={ErrorView} />
      </Switch>
    </ContentWrapper>
    <Footer>
      <FooterText>{'Eco chain blog'}</FooterText>
    </Footer>
  </LayoutWrapper>
);

export default App;
