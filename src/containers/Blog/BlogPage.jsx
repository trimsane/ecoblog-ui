import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';

import { searchBlogItems, fetchBlogItemsPage } from '../../actions/blogActions';

import { List, Row, Col, Menu, Pagination } from 'antd';
import ArticleListItem from '../../components/ArticleListItem';
import DelayedSearch from '../../components/DelayedSearch';
import ErrorView from '../../components/ErrorView';

import { Link } from 'react-router-dom';

class Blog extends Component {
  componentDidMount() {
    const { pages } = this.props;
    this.props.fetchBlogItemsPage(pages.selected, pages.size);
  }

  render() {
    const { error, loading, items, pages, search } = this.props;
    
    if (error) {
      return <ErrorView msg={error} />;
    }

    return (
      <Row type="flex" justify="center" gutter={16}>
        <Col span={15} order={1}>
          <List
            dataSource={loading ? new Array(pages.size) : items}
            renderItem={blogItem => (
              <ArticleListItem loading={loading} item={blogItem} />
            )}
          />
          <Pagination
            current={pages.selected}
            pageSize={pages.size}
            total={pages.total}
            onChange={this.handlePageClick}
          />
        </Col>
        <Col span={6} order={2}>
          <Row type="flex" justify="center">
            <DelayedSearch delay={100} onChange={this.props.searchBlogItems} />
            <Menu defaultOpenKeys={['sub1']} mode="inline">
              {search.query === ''
                ? this.renderMenu(items)
                : this.renderMenu(search.results)}
            </Menu>
          </Row>
        </Col>
      </Row>
    );
  }

  renderMenu = items =>
    items &&
    items.map((item, i) => (
      <Menu.Item key={i}>
        <Link to={`/articles/${item.id}`}>{item.title}</Link>
      </Menu.Item>
    ));

  handlePageClick = page => {
    this.props.push(`${page}`);
    this.props.fetchBlogItemsPage(page, this.props.pages.size);
  };
}

const mapStateToProps = ({ routing, blog }, { match }) => {
  const { items, search } = blog;

  const currentPage = parseInt(match.params.page);

  const selected = items.meta.page;

  const payload = items[selected];

  return {
    routing,
    items: payload && payload.data,
    loading: payload && payload.loading,
    error: (isNaN(currentPage) && 'invalid page') || (payload && payload.error),
    pages: {
      selected: currentPage,
      total: items.meta.total,
      size: items.meta.pageSize
    },
    search: search
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchBlogItemsPage,
      searchBlogItems,
      push
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog);
