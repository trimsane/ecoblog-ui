import React, { Component, Fragment } from 'react';
import { goBack } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Row, Col, Skeleton, Spin } from 'antd';
import { fetchArticle } from '../../actions/blogActions';
import ErrorView from '../../components/ErrorView';
import { Title, Timestamp, Content } from '../../components/styles';
import { timestampToDateString } from '../../utils/date';

const dummyArticle = {
  title: '',
  createdAt: Date(),
  content: ''
};

class ArticlePage extends Component {
  componentDidMount() {
    const { id } = this.props;

    this.props.fetchArticle(id);
  }

  render() {
    const { article, loading, error } = this.props.selected;

    if (error) {
      return <ErrorView msg={error} />;
    }

    return (
      <Fragment>
        {loading ? (
          <Skeleton loading active paragraph={{ rows: 15 }} />
        ) : (
          this.renderContent(article)
        )}
      </Fragment>
    );
  }

  renderContent(article) {
    const item = article || dummyArticle;
    const { title, createdAt, intro, content } = item;

    return (
      <Fragment>
        <Row type="flex" justify="space-between" align={'center'}>
          <Col span={4}>
            <Title>{title}</Title>
          </Col>
          <Col span={4}>
            <Timestamp>{timestampToDateString(createdAt)}</Timestamp>
          </Col>
        </Row>
        <Row>
          <Content>{intro}</Content>
          <Content>{content}</Content>
          <Button onClick={this.props.goBack}>Go back</Button>
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ routing, blog }, { match }) => ({
  routing,
  id: match.params.id,
  selected: blog.selected
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      goBack: () => dispatch(goBack()),
      fetchArticle
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticlePage);
