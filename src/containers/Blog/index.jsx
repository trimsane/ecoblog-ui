import BlogPage from './BlogPage';
import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import ArticlePage from './ArticlePage';

const BlogRouter = () => {
  return (
    <Switch>
      <Redirect exact path='/blog' to={'blog/pages/1'} />
      <Route exact path={'/blog/pages/:page'} component={BlogPage} />
      <Route exact path={'/articles/:id'} component={ArticlePage} />
    </Switch>
  );
};

export default BlogRouter;
