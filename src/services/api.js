const API_END_POINT = 'http://localhost:7070';
const API_V1 = '/api/v1';

const BLOGS_URL = `${API_END_POINT}${API_V1}/blog-items`
const headers = {
  'Content-Type': 'application/json; charset=utf-8'
};



export const fetchBlogItems = (size, skip) =>
  fetchGET('GET', `${BLOGS_URL}?limit=${size}&skip=${skip}&fields=id,createdAt,title,intro`);

export const searchBlogItems = query =>
  fetchGET('GET', `${BLOGS_URL}/search?query=${query}&limit=${10}&fields=id,title`);

export const fetchBlogItem = id =>
  fetchGET('GET', `${BLOGS_URL}/${id}`);

const fetchGET = (method, url, data) =>
  fetch(url, {
    mode: 'cors',
    method,
    headers,
    data: JSON.stringify(data)
  }).then(res => res.json());
